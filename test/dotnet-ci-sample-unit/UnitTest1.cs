using System;
using Xunit;

namespace dotnet_ci_sample_unit
{
    public class MyClass
    {
        public static string Say(string name)
            => $"Hello {name}!";
    }
    
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var expected = "Hello yuta!";
            var actual = MyClass.Say("yuta");
            Assert.Equal(expected, actual);
        }
    }
}
